// getTODO 

const { response } = require('express');
const Usuario = require('../models/usuario');
const Libro = require('../models/libro');
const Hospital = require('../models/hospital');


const getTodo = async(req, res = response) => {
	const busqueda = req.params.busqueda;
	//Este regEx es para buscar una expresion exacta en un buscador
	const regex = new RegExp(busqueda, 'i');

	// const usuarios = await Usuario.find({nombre: regex});
	const [ usuarios,libros, hospitales ] = await Promise.all([
    Usuario
      .find({nombre: regex}),
		Libro.find({nombre: regex}),
		Hospital.find({nombre: regex}),
  ]);

	try { 
		res.json({
			ok: true,
			msg: 'TODO',
			busqueda,
			usuarios,
			libros,
			hospitales,
		});
	} catch (error) {
		console.log(error);
		res.status(500).json({
			ok: false,
      msg: 'Hable Con el Administrador'
		});
	}
};


const getDocumentosColeccion = async(req, res = response) => {

	//Esta busqueda esta segmentada
	const tabla = req.params.tabla;

	const busqueda = req.params.busqueda;

	const regex = new RegExp(busqueda, 'i');

	let data = [];

	try {
		switch (tabla) {
			//Antes de cada busqueda puedes havcerle un populate que esta arriba como ejemñplo
			case 'libros':
				data  =  await Libro.find({nombre: regex})
																.populate('usuario', 'nombre img')
																.populate('hospital', 'nombre img');
				break;
			case 'hospitales':
				data  =  await Hospital.find({nombre: regex})
																.populate('usuario', 'nombre img');
					break;
			case 'usuarios':
				data  =  await Usuario.find({nombre: regex});
					break;
			default:
				return res.status(400).json({
					ok:false,
					msg: 'La tabla tiene que ser Usuarios, Libros u Hospitales'
				});
		}
		res.json({
			ok: true,
			resultados: data
		});
	} catch (error) {
		console.log(error);
		res.status(500).json({
			ok: false,
      msg: 'Hable Con el Administrador'
		});
	}
};

module.exports = {
	getTodo,
	getDocumentosColeccion
}