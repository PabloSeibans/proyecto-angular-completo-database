const { response } = require('express');
const Libro = require('../models/libro');

const getLibros = async(req, res = response) => {

	//Los Medicos van a ser los libros

	const libros = await Libro.find()
															.populate('usuario', 'nombre img')
															.populate('hospital', 'nombre img');

	res.json({
		ok: true,
		libros
	})
};


const getLibroById = async(req, res = response) => {

	const id = req.params.id;

	//Los Medicos van a ser los libros

	try {
		const libro = await Libro.findById(id)
															.populate('usuario', 'nombre img')
															.populate('hospital', 'nombre img');

		res.json({
			ok: true,
			libro
		})
	} catch (error) {
		console.log(error);
		res.status(404).json({
			ok: false,
			msg: 'Hable con el Administrador'
		})
	}
};

const crearLibro = async(req, res = response) => {

	const uid = req.uid;
	const libro = new Libro({
		usuario: uid,
		...req.body
	});

	try {

		const libroDB = await libro.save();

		res.json({
			ok: true,
			libro: libroDB
		});

	} catch (error) {
		console.log(error);
		res.status(500).json({
			ok: false,
      msg: 'Hable Con el Administrador'
		});
	}
};

const actualizarLibro = async (req, res = response) => {
	const id = req.params.id;
	const uid = req.uid;

	try {
		const libro = await Libro.findById(id);

		if (!libro) {
			return res.status(404).json({
				ok: true,
				msg: 'libro no encontrado por id'
			});
		}

		const cambiosLibro = {
			...req.body,
			usuario: uid
		}
		

		const libroActualizado = await Libro.findByIdAndUpdate(id, cambiosLibro, {new: true});



		res.json({
			ok: true,
			libro: libroActualizado,
		})
	} catch (error) {
		res.status(500).json({
			ok: false,
			msg: 'Error al actualizar Libro'
		})
	}
};

const borrarLibro = async (req, res = response) => {
	const id = req.params.id;

	try {
		const libro = await Libro.findById(id);

		if (!libro) {
			return res.status(404).json({
				ok: true,
				msg: 'libro no encontrado por id'
			});
		}

		await Libro.findByIdAndDelete(id)

		res.json({
			ok: true,
			msg: 'Libro borrado'
		})
	} catch (error) {
		res.status(500).json({
			ok: false,
			msg: 'Error al Eliminar Libro'
		})
	}
};

module.exports = {
	getLibros,
	crearLibro,
  actualizarLibro,	
	borrarLibro,
	getLibroById	
}