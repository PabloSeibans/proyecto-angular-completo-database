const path = require('path');
const fs = require('fs');
//Este path ya viene en node y me servira para construir un path completo
const {response} = require('express');
const {v4: uuidv4} = require('uuid');
const { actualizarImagen } = require('../helpers/actualizar-imagen');
require('dotenv').config();

const fileUpload = (req, res = response ) => {


	const tipo = req.params.tipo;
	const id = req.params.id;

	const tiposValidos = ['hospitales', 'libros', 'usuarios'];

	//Esto controla los pats que son accesibles
	if( !tiposValidos.includes(tipo) ){
		res.status(400).json({
      ok: false,
			msg: 'No es un libro, usuario u hospital (tipo)',
		});
	};

	//Se pueden recibir varias imagenes que serian las de eventos
	// console.log(req.files);

	//Validar que exista un archivo 
	if( !req.files || Object.keys(req.files).length === 0 ){
		return res.status(400).json({
      ok: false,
			msg: 'No hay ningún Archivo',
		});
	};

	//Validacion de tamaño de la imagen
	if(req.files.imagen.truncated){
		return res.status(400).json({
			ok: false,
			msg: `El archivo es demasiado grande, solo se permiten archivos de ${process.env.MAXSIZEUPLOAD}MB`
		});
	};

	//Procesar la imagen
	const file = req.files.imagen;
	const nombreCortado = file.name.split('.');
	const extensionArchivo = nombreCortado[nombreCortado.length - 1];


	//Validar extensión
	const extensionesValidas = ['png', 'jpg', 'jpeg', 'gif'];
	if(!extensionesValidas.includes(extensionArchivo) ){
		return res.status(400).json({
      ok: false,
			msg: 'No es un archivo valido',
		});
	};

	//Generar el nombre del archivo
	const nombreArchivo = `${uuidv4()}.${extensionArchivo}`;

	//Path para guardar la imagen
	const path = `./uploads/${tipo}/${nombreArchivo}`;


	// Use the mv()  para mover la imagen de cualquier lugar
	file.mv( path , (err) => {
		if (err) {
			console.log(err);
			return res.status(500).json({
				ok: false,
				msg: 'Error al mover el archivo',
			});
		}


		//Actuaizar la base de datos
		actualizarImagen( tipo, id, nombreArchivo );

		res.json({
			ok: true,
			msg: 'Archivo subido',
			nombreArchivo
		});
	});
}

const retornaImagen = (req, res = response) => {
	const tipo = req.params.tipo;
  const foto = req.params.foto;

  const pathImg = path.join( __dirname, `../uploads/${tipo}/${foto}` );


	// Imagen por Defecto
	if(fs.existsSync(pathImg)){
		res.sendFile(pathImg);
	} else {
		const pathImg = path.join( __dirname, `../uploads/imagen.jpg` );
		res.sendFile(pathImg);
	}
};

module.exports = {
  fileUpload,
	retornaImagen
}