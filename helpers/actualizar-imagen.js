const fs = require('fs');

const Usuario = require('../models/usuario');
const Libro = require('../models/libro');
const Hospital = require('../models/hospital');


const borrarImagen = (path) => {
	if (fs.existsSync(path)) {
		//Borrar la imagen Anterior 
		fs.unlinkSync(path);
	}
}

const actualizarImagen = async (tipo, id, nombreArchivo) => {

	let pathViejo = '';

	switch (tipo) {
		case 'libros':
			const libro = await Libro.findById(id);
			if (!libro) {
				console.log('No es un Libro por Id');
				return false;
			}

			pathViejo = `./uploads/libros/${libro.img}`;

			borrarImagen(pathViejo);

			libro.img = nombreArchivo;
			await libro.save();
      return true;
			break;
		case 'hospitales':
			const hospital = await Hospital.findById(id);
			if (!hospital) {
				console.log('No es Hospital por Id');
				return false;
			}

			pathViejo = `./uploads/hospitales/${hospital.img}`;

			borrarImagen(pathViejo);

			hospital.img = nombreArchivo;
			await hospital.save();
      return true;
		
			break;
		case 'usuarios':
			const usuario = await Usuario.findById(id);
			if (!usuario) {
				console.log('No es Usuario por Id');
				return false;
			}

			pathViejo = `./uploads/usuarios/${usuario.img}`;

			borrarImagen(pathViejo);

			usuario.img = nombreArchivo;
			await usuario.save();
      return true;
			break;
		default:
			break;
	}
}

module.exports = {
	actualizarImagen
}