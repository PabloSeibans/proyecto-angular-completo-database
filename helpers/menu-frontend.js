const getMenuFront = (role = 'USER_ROLE') => {
	const menu = [
    {
      titulo: 'Principal',
      icon: 'mdi mdi-gauge',
      submenu: [
        {titulo: 'Main', url: '/'},
        {titulo: 'Gráficas', url: 'grafica1'},
        {titulo: 'Rxjs', url: 'rxjs'},
        {titulo: 'Promesas', url: 'promesas'},
        {titulo: 'ProgresBar', url: 'progress'},
      ]
    },
    {
      titulo: 'Mantenimiento',
      icon: 'mdi mdi-folder-lock-open',
      submenu: [
        {titulo: 'Hospitales', url: 'hospitales'},
        {titulo: 'Libros', url: 'libros'},
      ]
    }
  ];

	if(role === 'ADMIN_ROLE'){
		menu[1].submenu.unshift({titulo: 'Usuarios', url: 'usuarios'})
	}

	return menu;
}

module.exports = {
	getMenuFront
}