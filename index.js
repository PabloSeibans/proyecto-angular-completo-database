require('dotenv').config();

const express = require('express');
const cors = require('cors');

const { dbConnection } = require('./database/config');

//* Crear el Servidor de Express
const app = express();

//* Cors
//* El cors es para hacer nuestra api accesible a diferentes usuarios
app.use(cors());
//* Lectura y Parseo del Body
app.use(express.json());
//* Base de Datos
dbConnection();

//Directorio Publico
app.use(express.static('public'));

app.use('/api/usuarios', require('./routes/usuarios') );
app.use('/api/hospitales', require('./routes/hospitales') );
app.use('/api/libros', require('./routes/libros') );
app.use('/api/login', require('./routes/auth') );
app.use('/api/todo', require('./routes/busquedas') );
app.use('/api/upload', require('./routes/uploads') );
app.use('/api/eventos', require('./routes/eventos') );

app.listen( process.env.PORT, () => {
	console.log('Servidor corriendo en el puerto '+ process.env.PORT);
});

//TODO la informacion importante
//* cors es para acer accesible nuestra api a varios dispositivos
//* cuando subes una imagen recibes un truncate que te indica si la imagen a sido recortada para poder
//* guardarse en la base de datos

//TODO AUN FALTA IMPLEMENTAR LA RUTA DE LOS EVENTOS

//TODO Falta corregir el hecho de que si mi ruta esta bien al subirt
//una imagen debo controlar mejor el id porque igual lo guarda