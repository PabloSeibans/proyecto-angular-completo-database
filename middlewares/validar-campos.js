const { response } = require('express');
const { validationResult } = require('express-validator')

const validarCampos = (req, res = response, next) => {


  const errores = validationResult(req);
  //pregunta si no etsa vacio significa que hay errores
  if (!errores.isEmpty()) {
    return res.status(400).json({
      ok: false,
      errors: errores.mapped()
    });
  }

  next();
}


module.exports = {
  validarCampos
}