const { Schema, model } = require('mongoose');

const EventoSchema = Schema(
  {
    nombre: {
      type: String,
      required: true
    },
    img: {
      type: String
    },
		usuario: {
      required: true,
			type: Schema.Types.ObjectId,
			ref: 'Usuario'
		},
    detalles: {
			type: String,
		}
  }, {
    versionKey: false,
    collection: 'eventos'
  });

EventoSchema.method('toJSON', function() {
  const {...object } = this.toObject();
  return object;
})

module.exports = model('Evento', EventoSchema);