const { Schema, model } = require('mongoose');

const LibroSchema = Schema(
  {
    nombre: {
      type: String,
      required: true
    },
    // precio: {
    //   type: Number,
    //   required: true
    // },
    // detalle: {
    //   type: String,
    //   required: true
    // },
    // fecha: {
    //   type: Date,
    //   required: true
    // },
    img: {
      type: String
    },
		usuario: {
			type: Schema.Types.ObjectId,
			ref: 'Usuario',
      required: true
		},
		hospital: {
			type: Schema.Types.ObjectId,
			ref: 'Hospital',
      required: true
		},

  },{
    versionKey: false,
    collection: 'libros'
});

LibroSchema.method('toJSON', function() {
  const { ...object } = this.toObject();
  return object;
})

module.exports = model('Libro',LibroSchema);