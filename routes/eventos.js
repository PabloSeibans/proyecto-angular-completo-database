/*
	Ruta: /api/eventos
*/

const { Router } = require('express');
const { check } = require('express-validator'); 
const { validarCampos } = require('../middlewares/validar-campos')

const { validarJWT } = require('../middlewares/validar-jwt');

const {
	getEventos,
	crearEvento,
	actualizarEvento,
  borrarEvento
} = require('../controllers/eventos-controllers');

const router = Router();

router.get( '/', getEventos);
//Lo que esta en el array es un middleware
//Leer la documentación de ExpressValidator
router.post( '/',
    [
      validarJWT,
      check('nombre', 'El nombre del Hospital es Necesario').not().isEmpty(),
      validarCampos
    ],
    crearEvento);

router.put( '/:id',
    [],
    actualizarEvento);

router.delete('/:id', borrarEvento);
 
module.exports = router;