/*
	Ruta: /api/medicos
*/

const { Router } = require('express');
const { check } = require('express-validator'); 
const { validarCampos } = require('../middlewares/validar-campos')

const { validarJWT } = require('../middlewares/validar-jwt');

const {
	getLibros,
	crearLibro,
    actualizarLibro,	
	borrarLibro,
    getLibroById
} = require('../controllers/libros-controllers');

const router = Router();

router.get( '/',validarJWT,getLibros);

//Lo que esta en el array es un middleware
//Leer la documentación de ExpressValidator
router.post( '/',
    [
        validarJWT,
        check('nombre', 'El nombre del Libro es Necesario').not().isEmpty(),
        check('hospital', 'El id del Hospital debe ser Válido').isMongoId(),
        validarCampos
    ],
    crearLibro
);

router.put( '/:id',
    [
        validarJWT,
        check('nombre', 'El nombre del Libro es Necesario').not().isEmpty(),
        check('hospital', 'El id del Hospital debe ser Válido').isMongoId(),
        validarCampos
    ],
    actualizarLibro
);

router.delete('/:id',
    validarJWT,
    borrarLibro
);

router.get('/:id',
    validarJWT,
    getLibroById
);
 
module.exports = router;